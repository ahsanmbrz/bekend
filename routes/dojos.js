const express = require('express');
const router = express.Router();
const dojoController = require('../app/api/controllers/dojos');


router.get('/', dojoController.getAll);
router.get('/:dojoId', dojoController.getById);

module.exports = router;