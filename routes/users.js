const express = require('express');
const router = express.Router();
const userController = require('../app/api/controllers/users');

const multer = require('multer');
const path = require('path');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/images/');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname);
  }
});
var upload = multer({ storage: storage });
router.use('/static', express.static(path.join(__dirname, 'public')))

router.post('/register', userController.create);
router.post('/authenticate', userController.authenticate);
router.post('/upload', upload.single('wallpaper'), function (req, res) {
  var imagePath = req.file.path.replace(/\\/g, "/");
  var hasil = imagePath.replace('public/',"");
  res.json({'status':200,'success':true,'address':'http://192.168.0.22:3000/static/'+hasil});
});

module.exports = router;