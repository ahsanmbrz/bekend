const express = require('express');
const router = express.Router();
const loginController = require('../app/api/controllers/logins');


router.post('/auth', loginController.get);
router.post('/add', loginController.insert);
module.exports = router;