const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const LoginSchema = new Schema({
	email:{
        type:String,
        required:true,
        unique: true
    },
    nama:String,
    phone:String
});



module.exports = mongoose.model('Login', LoginSchema)