const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const DojoSchema = new Schema({
	latlong:{
		latitude:Number,
        longitude:Number
    },
    name:String,
    alamat:String,
    image:String,
    fasilitas:[String],
    visi:String,
    misi:String,
    lain:String
});



module.exports = mongoose.model('Dojo', DojoSchema)