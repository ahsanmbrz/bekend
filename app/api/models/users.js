const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;

//Define a schema
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	registeredby:{
		role:String,
		id:String 
	},
	nama:String,
	tempatlahir:String,
	tanggallahir:String,
	jeniskelamin:String,
	provinsi:String,
	kota:String,
	kec:String,
	kel:String,
	alamat:String,
	islemkari:{
		type:Boolean,
		default:false
	},
	isbeladirilain:{
		type:Boolean,
		default:false
	},
	perguruan:String,
	pengprov:String,
	pengcab:String,
	exdojo:String,
	dojo:String,
	sabuk:String,
	pengalamanlain:String,
	photo:String,
	sabuk:String,
	verified:{
        type:Boolean,
        default:false
    }
});

UserSchema.pre('save', function(next){
// this.password = bcrypt.hashSync(this.password, saltRounds);
next();
});

module.exports = mongoose.model('User', UserSchema);