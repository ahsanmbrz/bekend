const dojoModel = require('../models/dojos');					

module.exports = {
	getById: function(req, res, next) {
		dojoModel.findById(req.params.dojoId, function(err, movieInfo){
			if (err) {
				next(err);
			} else {
				res.json({status:"success", message: "Movie found!!!", data:{movies: movieInfo}});
			}
		});
	},

	getAll: function(req, res, next) {
		let dojoList = [];

		dojoModel.find({}, function(err, dojos){
			if (err){
				next(err);
			} else{
				for (let dojo of dojos) {
					dojoList.push({id: dojo._id, latlong: dojo.latlong, name: dojo.name, alamat:dojo.alamat, image:dojo.image, 
						fasilitas:dojo.fasilitas, visi:dojo.visi, misi:dojo.misi,lain:dojo.lain});
				}
				res.json({status:"success", message: "Dojo List Found", data: dojoList});
							
			}
		});
	},
	// insertanggota: function(req, res, next) {
	// 	dojoModel.update(
	// 	   { _id: req.body.id },
	// 	   {$push:{"pendaftar":req.body.data}},{safe: true, upsert: true}, function(err, dojos){
	// 		if (err){
	// 			next(err);
	// 		} else{
	// 			res.json({status:"success", message: "Dojo List Found", data: dojos});
							
	// 		}
	// 	});
	// },
}