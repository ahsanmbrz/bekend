const loginModel = require('../models/logins');					

module.exports = {
	get: function(req, res, next) {
		loginModel.findOne({email:req.body.email}, function(err, result){
			if (err) {
				next(err);
			} else {
				if(result==null){
					res.json({status:"failed"});
				}else{
					res.json({status:"success", result});
				}
				
			}
		});
	},
	insert:function(req, res, next){
		loginModel.create({
			email:req.body.email,
			nama:req.body.nama,
			phone:req.body.phone
		},function(err, result){
			if(err){
				next(err);
			}else{
				res.json({status:"success",id:result});	
			}
		});
	},

}