const userModel = require('../models/users');
const bcrypt = require('bcrypt');	
const jwt = require('jsonwebtoken');				

module.exports = {
	create: function(req, res, next) {
		var result = req.body;
		userModel.collection.insertMany(result, function (err, docs) {
	      if (err){ 
	          return console.error(err);
	      } else {
	        res.json({status:"success",message:"documents inserted"})
	      }
	    });
	},

	authenticate: function(req, res, next) {
		userModel.findOne({username:req.body.username}, function(err, userInfo){
					if (err) {
						next(err);
					} else {

						if(userInfo != null && bcrypt.compareSync(req.body.password, userInfo.password)) {

						 const token = jwt.sign({id: userInfo._id}, req.app.get('secretKey'), { expiresIn: '1h' }); 

						 res.json({status:"success", message: "user found!!!", data:{user: userInfo, token:token}});	

						}else{

							res.json({status:"error", message: "Invalid email/password!!!", data:null});

						}
					}
				});
	},
}			
